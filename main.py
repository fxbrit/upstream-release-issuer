import requests
import re
from bs4 import BeautifulSoup
from packaging import version

def main():
    project_id = 13852982 # project id of the common repo of librewolf
    token = "" # insert your personal access token here
    label = "update::Upstream" # label specific for issues created by this script

    last_issue = requests.get("https://gitlab.com/api/v4/projects/%s/issues?labels=%s" %(project_id, label))
    releases = requests.get("https://archive.mozilla.org/pub/firefox/releases/")

    if last_issue.status_code == 200 :
        try :
            last_issue_version = last_issue.json()[0]["title"].split(", ")[1] # FF version from the last opened issue
        except:
            last_issue_version = "0.0.0" # if no issue exists with the default syntax, always create a new one
    
    if releases.status_code == 200 :
        releases.encoding = releases.apparent_encoding
        soup = BeautifulSoup(releases.content, "lxml")
        tags = soup.find_all("a")
        versions = []

        for a in soup.find_all("a") :
            ver = str(a.contents[0][:-1]) # strip last char which is always '/'
            if re.match("[\d.]+$", ver) and ver != "." :
                versions.append(version.parse(ver)) # append versions which are not beta, exclude directories in the archive

        versions.sort()
        latest = versions[-1]
        
        if latest > version.parse(last_issue_version) : # if an issue for the same version exists do not create a new one
            title = "Upstream release by Mozilla, " + str(latest)
            body = "Mozilla has released the source code for version " + str(latest) + " of Firefox."
            url="https://gitlab.com/api/v4/projects/%s/issues?title=%s&description=%s&labels=%s" %(project_id, title, body, label)
            response = requests.post(url, headers={"PRIVATE-TOKEN": token})
            print(response.status_code)

main()